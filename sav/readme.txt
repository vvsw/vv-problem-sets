# c.hogyun@gmail.com

# Problem

At 12:00am each morning you are giving a list of timestamps for that day separated by a comma. Some timestamps may be different and some may be at the same time. Please write a Python script that sends an API call at each of the timestamps, accurate down to the second. If multiple timestamps are at the same time, the API calls must be sent at the same time. List of Times for a Given Day (24hr Hour:Minutes:Seconds): 09:15:25,11:58:23,13:45:09,13:45:09,13:45:09,17:22:00,17:22:00 API Call to Send GET request to "ifconfig.co" Please send a Zip file that contains the python code and a readme on how to execute. You can import any native python packages (ex: datetime, urllib, etc...) you need but no 3rd party packages.*

## Requirements

python3.7+


## Usage

```
python3 timestamp_api_caller.py

```

## Configuration

edit the header vars in timestamp_api_caller.py to change default configuration values

```
API_TARGET_URL = "http://ifconfig.co"
DEFAULT_PUBLISH_TIME_STRING = "12:00:00"
DEBUG_VERBOSE_PRINT = True
DEBUG_PRINT_STATE = True
TEST_MODE_ENABLED = True
```

(!) if neccessary, the `receive_timestamp_string()` can be redefined by user to generate or get the timestamps from custom external source 


```
def receive_timestamp_string():
	# user defined, must return string of timestamps separated by commas
	return ""
```


## Example Output of execution

```
[DEBUG]: current_time_is = 23:44:12
[DEBUG]: update time is = 23:44:31
[DEBUG]: times equal check = False
[DEBUG]: new_list_received = True
[DEBUG]: unique_timestamps_dict = {'23:44:16': 4, '23:44:17': 2, '23:44:18': 4, '23:44:19': 2, '23:44:20': 2, '23:44:23': 1, '23:44:24': 2, '23:44:25': 3}

```

```
$ python3 timestamp_api_caller_.py 
ret = <Response [200]>
ret = <Response [200]>
ret = <Response [200]>
ret = <Response [200]>
ret = <Response [200]>
ret = <Response [200]>
ret = <Response [200]>
```