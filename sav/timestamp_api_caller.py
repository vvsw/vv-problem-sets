
#!/bin/python3

# hgc 2021 april

import concurrent.futures
import datetime
from random import randrange
import requests
import time
import _thread

API_TARGET_URL = "http://ifconfig.co"
DEFAULT_PUBLISH_TIME_STRING = "12:00:00"
DEBUG_VERBOSE_PRINT = True
DEBUG_PRINT_STATE = True
TEST_MODE_ENABLED = False


# NEW_TIMESTAMPS_PUBLISH_TIME = datetime.datetime.strptime("17:22:11", '%H:%M:%S').strftime('%H:%M:%S')  # check for new list of timestamps at 12 pm daily
time_change = datetime.timedelta(seconds=5)
NEW_TIMESTAMPS_PUBLISH_TIME = (datetime.datetime.now() + time_change).strftime('%H:%M:%S')

if TEST_MODE_ENABLED:
	NEW_TIMESTAMPS_PUBLISH_TIME = (datetime.datetime.now() + time_change).strftime('%H:%M:%S')
else:
	NEW_TIMESTAMPS_PUBLISH_TIME = datetime.datetime.strptime(DEFAULT_PUBLISH_TIME_STRING, '%H:%M:%S').strftime('%H:%M:%S')

# user can redefine this function
# fn() -> String
def receive_timestamp_string():
	list_of_times = []
	for i in range(0, 10):
		time_change = datetime.timedelta(seconds=5 + i)
		for i in range(0, randrange(5)):
			list_of_times.append( (datetime.datetime.now() + time_change).strftime('%H:%M:%S') )
	_dprint("generated list of times = {}".format(",".join(list_of_times)))
	return ",".join(list_of_times) #"09:15:25,11:58:23,13:45:09,13:45:09,13:45:09,17:22:00,17:22:00"


def _http_get_request( url ):
	ret = requests.get(API_TARGET_URL)
	print("ret = {}".format(str(ret)))
	return ret

def _dprint(s):
	if DEBUG_VERBOSE_PRINT:
		print("[DEBUG]: {}".format(s))

# return dict of timestamps and their respective counts
def get_dict_of_timestamps():
	# convert given timestamps string to list via String.split()
	example_given_timestamps_string = receive_timestamp_string()
	example_given_timestamps_list = example_given_timestamps_string.split(",")

	# ensure that result is a list and non-empty
	assert( type( example_given_timestamps_list ) == list 
		and len( example_given_timestamps_list ) > 0 )

	# keep track of how many unique timestamps there are and number of instances
	unique_timestamps_dict = {}  
	for timestamp in example_given_timestamps_list:
		_dprint("timestamp={}".format(timestamp))
		date_time_obj = timestamp #datetime.datetime.strptime(timestamp, '%H:%M:%S')  # 24hr Hour:Minutes:Seconds
		if date_time_obj not in unique_timestamps_dict:
			# found first instance of a unique timestamp
			# instead of storing str of timestamp, store actual timestamp obj for the key
			unique_timestamps_dict[date_time_obj] = 1
		else:
			unique_timestamps_dict[date_time_obj] += 1
	_dprint(unique_timestamps_dict)
	return unique_timestamps_dict

# main

new_list_received = False


unique_timestamps_dict = {}
while True:
    current_date_timestamp = datetime.datetime.now().strftime('%H:%M:%S')
    if DEBUG_PRINT_STATE:
	    _dprint("current_time_is = {}".format(str(current_date_timestamp)))
	    _dprint("update time is = {}".format(NEW_TIMESTAMPS_PUBLISH_TIME))
	    _dprint("times equal check = {}".format(  current_date_timestamp == NEW_TIMESTAMPS_PUBLISH_TIME ))
	    _dprint("new_list_received = {}".format( new_list_received ))
	    _dprint("unique_timestamps_dict = {}\n".format(unique_timestamps_dict))

    # check for new timestamps at 12:00:00 PM
    # edge case where multiple different same times for 12:00:00 PM due to miliseconds
    if new_list_received == False and current_date_timestamp == NEW_TIMESTAMPS_PUBLISH_TIME:
        _dprint( str(unique_timestamps_dict ))
        assert( unique_timestamps_dict == {} )
        unique_timestamps_dict = get_dict_of_timestamps()
        new_list_received = True
        _dprint("timestamps have been updated!")

        if TEST_MODE_ENABLED:
            time_change = datetime.timedelta(seconds=20)
            NEW_TIMESTAMPS_PUBLISH_TIME = (datetime.datetime.now() + time_change).strftime('%H:%M:%S')


    current_time_string = datetime.datetime.now().strftime('%H:%M:%S')
    if current_time_string in unique_timestamps_dict:
        # account for multiple calls ;)
        for i in range(0, unique_timestamps_dict[current_time_string] ):
            try:
            	_thread.start_new_thread( _http_get_request, ( API_TARGET_URL, ) )
            except Exception as e:
                _dprint ("Error: {}".format(e))
                
        del unique_timestamps_dict[ current_time_string ]
        # we have sent all the required timestamp api calls, wait for next update
        if len( unique_timestamps_dict ) == 0:
        	new_list_received = False